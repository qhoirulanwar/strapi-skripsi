module.exports = {
    //...
    settings: {
        cors: {
            //   origin: ['http://localhost', 'https://mysite.com', 'https://www.mysite.com'],
            // origin: '*',
            // methods: ['GET,PUT,POST,DELETE,PATCH,OPTIONS'],
            // headers: ["Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization", "X-Frame-Options"]
            headers: ["ultrablock-udid", "ultrablock-version", "Content-Type", "Authorization"]
        },
    },
    options: {
        tz: 'Asia/jakarta',
    },
};